au BufWinEnter,BufNewFile,BufRead *.md set filetype=markdown
au BufWinEnter,BufNewFile,BufRead *.py set filetype=python
au BufWinEnter,BufNewFile,BufRead *.{yaml,yml} set filetype=yaml
